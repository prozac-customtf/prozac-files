//-----------------------------------------------------------------------------------------//
// ReadMe for prozac files GitLab project (https://gitlab.com/prozac-customtf/prozac-files)
//-----------------------------------------------------------------------------------------//

This GitLab project just contains map-making-related files, to use with JACK.
Many of the files can be used on other map maker tools.

1.- Inside the "tools" folder there are the ericw tools. (QBSP, VIS and LIGHT executables)

2.- Inside the "wads" folder you can find "Q.wad" which contains all quake official textures
and "prozac.wad" which is a customized textures pack I use.

3.- Inside the "fgds" folder, there is the 3 FGD files (Forge Game Data) to use with current
Prozac mod. A "quake.fgd" for standard quake entities, a "fortress.fgd" with standard Team 
Fortress entities data, and a "prozac.fgd" with up-to-date entities information usable in
the current mod.

4.- Inside the "maps" folder, I will put all the test maps sources and (if I make any) other
maps. For use and be able to test in JACK.

//-----------------------------------------------------------------------------------------//
// ReadMe.txt originally written on 15/11/2021 by OfteN [cp]
//-----------------------------------------------------------------------------------------//
